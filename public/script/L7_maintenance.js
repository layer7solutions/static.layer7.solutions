init(function() {
    var seen = localStorage.getObject('maintenance_notice_seen_2018Feb16');
    if (seen) {
        return;
    }
    localStorage.setObject('maintenance_notice_seen_2018Feb16', true);
    app.dialog(`<div style="
        width: 400px;
        margin: 30px auto 40px;
        padding: 20px;
        background: #f7f9fb;
    "><strong style="
        font-size: 20px;
        text-align: center;
        display: block;
        margin-bottom: 8px;
    ">IMPORTANT NOTICE</strong><p style="
        font-size: 18px;
        line-height: 23px;
    ">TheSentinelBot and this website will both be temporarily unavailable for several hours this Friday at 8 PM Central for scheduled maintenance.<br/>We apologize for the inconvenience.</p>
    </div>`, app.DIALOG_MODAL)
});